let tabs = document.querySelectorAll('.tabs-title');
let tabContent = document.querySelectorAll('.tabs-content li');
tabs.forEach((tab) => {
    tab.addEventListener('click', () => {
        const active = document.querySelector('.active')
        active.classList.remove('active');
        tab.classList.add('active');
        tabContent.forEach(par => {
            par.classList.remove('active-content');
            par.classList.add('none');
            if (tab.dataset.item === par.dataset.paragraf) {
                par.classList.add('active-content');
            }
        })
    })
});
